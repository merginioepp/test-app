import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { FlightComponent } from './pages/flight/flight.component';
import { PassengerComponent } from './pages/passenger/passenger.component';
import { FlightFormComponent } from './forms/flight-form/flight-form.component';
import { PassengerFormComponent } from './forms/passenger-form/passenger-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  declarations: [
    AppComponent,
    FlightComponent,
    PassengerComponent,
    FlightFormComponent,
    PassengerFormComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    NgxDatatableModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
