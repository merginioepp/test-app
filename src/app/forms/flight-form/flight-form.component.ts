import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AbstractControl, FormControl, FormGroup, Validators } from '@angular/forms';
import { AppApiService } from 'src/app/app-api.service';

@Component({
  selector: 'app-flight-form',
  templateUrl: './flight-form.component.html',
  styleUrls: ['./flight-form.component.css']
})
export class FlightFormComponent implements OnInit {

  flight_form = new FormGroup({
    flight_number: new FormControl('',[Validators.required]),
    destination: new FormControl('', [Validators.required,])
  });

  pending_request:boolean = false;


  @Output() on_success = new EventEmitter();

  constructor(private _app_api_service: AppApiService) { }

  ngOnInit(): void {
  }

  submit() {

    this.pending_request = true;

    if(this.flight_form.invalid) {
      this.flight_form.markAllAsTouched();
      return;
  };

  this._app_api_service.AddFlight(+this.flight_form.get('flight_number')!.value!, this.flight_form.get('destination')!.value!)
    .subscribe(x => {

      this.pending_request = false;

      alert(`Flight successfully added.`);
      //emit data
      this.on_success.emit();

      this.flight_form.reset();
    }, err => {
      if (err.errors?.length > 0) {
        err.errors?.forEach(element => {

          this.flight_form.get(element.property)?.setErrors({ [element!.error_code]:  true});

        });
      }

      this.pending_request = false;
    });
  // this.on_submit.emit(this.form.value);

  // this._form.reset();
  // this.patch_value(this.form_default_value);
  }


  control_is_invalid(controlName: string, errorOnDirtyOnly = true) {
    let ctl = this.flight_form.get(controlName);
    if(!errorOnDirtyOnly) {
        return ctl!.touched && ctl!.invalid;
    }
    return (ctl!.dirty || ctl!.touched) && ctl!.invalid;
  }

  validation_error_msg(controlName: string, errorOnDirtyOnly = true) {
    let ctl = this.flight_form.get(controlName);
    if(!ctl) return '';
    if(ctl.errors?.required) {
      return `This field is required.`
    }

    if(ctl.errors?.idTaken) {
      return `Flight number is already taken.`
    }

    return `Value for this field is invalid.`;


//     message: `Apologies. The ${flight_class?.description} of Flight # ${this._passenger.flight_number} is full of passengers.`
// message: `The ${flight_class?.description} of Flight # ${this._passenger.flight_number} baggage limit of ${flight_class?.baggage_count_limit} per passenger has been exceeded.`
// message: `The ${flight_class?.description} of Flight # ${this._passenger.flight_number} baggages weight limit of ${flight_class?.baggage_weight_limit} per passenger has been exceeded.`
  }
}
