export class FlightClass {

  id: number;
  description: string;
  max_seats: number;
  offset_seat: number;
  baggage_count_limit: number;
  baggage_weight_limit: number;

  constructor(_id: number, _description: string, _max_seats: number, _offset_seat: number, _baggage_count_limit: number, _baggage_weight_limit: number) {
    this.id = _id;
    this.description = _description;
    this.max_seats = _max_seats;
    this.offset_seat = _offset_seat;
    this.baggage_count_limit = _baggage_count_limit;
    this.baggage_weight_limit = _baggage_weight_limit;
  }

  public static get() : FlightClass[] {
    let data: FlightClass[]  = [];

    data.push(new FlightClass(1, "Economy Class", 150, 50, 1, 20));
    data.push(new FlightClass(2, "Business Class", 30, 20, 2, 20));
    data.push(new FlightClass(3, "First Class", 20, 0, 2, 30));

    return data;
  }
}
