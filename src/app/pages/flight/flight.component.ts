import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormControlState } from '@angular/forms';
import { AppApiService } from 'src/app/app-api.service';

@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {


  rows: any = [];
  columns = [
    { prop: 'flight_number', name: 'Flight Number', width: 150 },
    { prop: 'destination', name: 'Vaccine Destination', width: 150 },
  ];

  constructor(private _app_api_service: AppApiService) {

  }

  ngOnInit(): void {
    this.rows = [...this._app_api_service._flights];
  }

  add_success(event) {
    // debugger
    this.rows = [...this._app_api_service._flights];
  }

}
