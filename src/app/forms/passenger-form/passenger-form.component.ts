import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { AppApiService } from 'src/app/app-api.service';
import { FlightClass } from 'src/app/models/flight-class.model';

@Component({
  selector: 'app-passenger-form',
  templateUrl: './passenger-form.component.html',
  styleUrls: ['./passenger-form.component.css']
})
export class PassengerFormComponent implements OnInit {


  flight_class_dropdown = FlightClass.get();

  flight_number_dropdown:any[] = [];

  passenger_form = new FormGroup({
    first_name: new FormControl('',[Validators.required]),
    last_name: new FormControl('', [Validators.required,]),
    id: new FormControl('',[Validators.required]),
    flight_class: new FormControl('', [Validators.required,]),
    payment_amount: new FormControl('',[Validators.required]),
    flight_number: new FormControl('', [Validators.required,]),
    number_of_bags: new FormControl('',[Validators.required]),
    total_weight: new FormControl('', [Validators.required,])
  });

  pending_request:boolean = false;


  @Output() on_success = new EventEmitter();

  constructor(private _app_api_service: AppApiService) { }

  ngOnInit(): void {

    this.flight_number_dropdown = this._app_api_service._flights;
  }

  submit() {
    //

    this.pending_request = true;

    if(this.passenger_form.invalid) {
      this.passenger_form.markAllAsTouched();
      return;
  };

  this._app_api_service.AddPassenger(
    this.passenger_form.get('first_name')!.value!,
    this.passenger_form.get('last_name')!.value!,
    +this.passenger_form.get('id')!.value!,
    +this.passenger_form.get('flight_class')!.value!,
    +this.passenger_form.get('payment_amount')!.value!,
    +this.passenger_form.get('flight_number')!.value!,
    +this.passenger_form.get('number_of_bags')!.value!,
    +this.passenger_form.get('total_weight')!.value!
    )
    .subscribe(x => {

      this.pending_request = false;


      alert(`Passenger ${x?.data?.first_name} ${x?.data?.last_name} successfully added. Seat number: ${x?.data?.passenger_seat}`);
      //emit data
      this.on_success.emit();

      this.passenger_form.reset();
    }, err => {

      if (err.errors?.length > 0) {
        err.errors?.forEach(element => {

          this.passenger_form.get(element.property)?.setErrors({ [element!.error_code]:  true});

        });
      }

      this.pending_request = false;
    });
  // this.on_submit.emit(this.form.value);

  // this._form.reset();
  // this.patch_value(this.form_default_value);
  }


  control_is_invalid(controlName: string, errorOnDirtyOnly = true) {
    let ctl = this.passenger_form.get(controlName);
    if(!errorOnDirtyOnly) {
        return ctl!.touched && ctl!.invalid;
    }
    return (ctl!.dirty || ctl!.touched) && ctl!.invalid;
  }

  validation_error_msg(controlName: string, errorOnDirtyOnly = true) {

    let ctl = this.passenger_form.get(controlName);

    let form_values = this.passenger_form.value;
    let flight_class = FlightClass.get().find(x => x.id == +form_values?.flight_class!);

    if(!ctl) return '';
    if(ctl.errors?.required) {
      return `This field is required.`
    }

    if(ctl.errors?.idTaken) {
      return `Passenger Id is already taken.`
    }

    if(ctl.errors?.passengerFull) {
      return `Apologies. The ${flight_class?.description} of Flight # ${form_values?.flight_number} is full of passengers.`;
    }

    if(ctl.errors?.baggageExceed) {
      return `The ${flight_class?.description} of Flight # ${form_values?.flight_number} baggage limit of ${flight_class?.baggage_count_limit} per passenger has been exceeded.`;
    }

    if(ctl.errors?.weightExceed) {
      return `The ${flight_class?.description} of Flight # ${form_values?.flight_number} baggages weight limit of ${flight_class?.baggage_weight_limit} per passenger has been exceeded.`;
    }




    return `Value for this field is invalid.`;


//     message: `Apologies. The ${flight_class?.description} of Flight # ${this._passenger.flight_number} is full of passengers.`
// message: `The ${flight_class?.description} of Flight # ${this._passenger.flight_number} baggage limit of ${flight_class?.baggage_count_limit} per passenger has been exceeded.`
// message: `The ${flight_class?.description} of Flight # ${this._passenger.flight_number} baggages weight limit of ${flight_class?.baggage_weight_limit} per passenger has been exceeded.`
  }
}
