import { Component, OnInit } from '@angular/core';
import { AppApiService } from 'src/app/app-api.service';
import { FlightClass } from 'src/app/models/flight-class.model';

@Component({
  selector: 'app-passenger',
  templateUrl: './passenger.component.html',
  styleUrls: ['./passenger.component.css']
})
export class PassengerComponent implements OnInit {

  rows: any = [];
  columns = [
    { prop: 'first_name', name: "First Name" },
    { prop: 'last_name', name: "Last Name" },
    { prop: 'id', name: "Id" },
    // { prop: 'flight_class', name: "Flight Class" },
    { prop: 'flight_class_display', name: "Flight Class" },
    { prop: 'payment_amount', name: "Payment Amount" },
    { prop: 'flight_number', name: "Flight Number" },
    { prop: 'number_of_bags', name: "Number of Bags" },
    { prop: 'total_weight', name: "Total Weight" },
    { prop: 'passenger_seat', name: "Passenger Seat No. #" },
  ];




  constructor(private _app_api_service: AppApiService) {

  }

  ngOnInit(): void {
    this.rows = this.map_lookup_fields();
  }

  add_success(event) {
    // debugger
    this.rows = this.map_lookup_fields();
  }

  map_lookup_fields() {
    let data: any[] = [...this._app_api_service._passengers];

    data.forEach(element => {
      element.flight_class_display = FlightClass.get().find(x => x.id == element.flight_class)?.description;
    });


    return data;
  }
}
