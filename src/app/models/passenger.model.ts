import { AppApiService } from "../app-api.service";
import { FlightClass } from "./flight-class.model";

export class Passenger {
  first_name: string | undefined;
  last_name: string | undefined;
  id: number | undefined;
  flight_class: number | undefined;
  payment_amount: number | undefined;
  flight_number: number | undefined;
  number_of_bags: number | undefined;
  total_weight: number | undefined;

  //additional field to determine seat number
  passenger_seat:number | undefined;


  constructor(_first_name, _last_name, _id, _flight_class, _payment_amount, _flight_number, _number_of_bags, _total_weight, _passenger_seat) {


    this.first_name = _first_name;
    this.last_name = _last_name;
    this.id = _id;
    this.flight_class = _flight_class;
    this.payment_amount = _payment_amount;
    this.flight_number = _flight_number;
    this.number_of_bags = _number_of_bags;
    this.total_weight = _total_weight;
    this.passenger_seat = _passenger_seat;
  }

}
