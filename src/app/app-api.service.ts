import { HttpClient } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { ActionResponse } from './models/action-reponse.model';
import { FlightClass } from './models/flight-class.model';
import { Flight } from './models/flight.model';
import { Passenger } from './models/passenger.model';
import { PassengerValidator } from './validators/passenger.validator';

@Injectable({
  providedIn: 'root'
})
export class AppApiService {

  // using arrays as temporary storage

  public _passengers: Passenger[] = [];
  public _flights: Flight[] = [];

  constructor() {
    //test data
    // this._flights.push(new Flight(100, 'Melbourne'));
    // this._flights.push(new Flight(101, 'Austrialia'))
    // this._flights.push(new Flight(120, 'Japan'));


    // for (let index = 0; index < 19; index++) {
    //   // const element = array[index];
    //   let val = index + 1;
    //   this._passengers.push(new Passenger(`TEST${val}`, `TEST${val}`, val , 3,  100, 100, 2, 20, val));

    // };


  }

  public AddFlight(id: number, destination: string) : Observable<ActionResponse<Flight>> {


    if (this._flights.some(x => x?.flight_number == id)) {

      // let error_res: ActionResponse<Flight> = {
      //   message: "Flight Id is already taken.",
      //   errors: [
      //     { property: "id", error_message: "Flight id is already taken."}
      //   ]

      // }

      let error_res = this.generate_response("Some values submitted are invalid.", null,[{ property: "flight_number", error_message: "Flight number is already taken.", error_code:"idTaken"}]);
      return throwError(error_res);
    }

    let flight: Flight = {
      flight_number: id,
      destination: destination
    };

    // let res: ActionResponse<Flight> = {
    //   message: "Flight successfully added.",
    //   data: flight,
    // };

    this._flights.push(flight);

    let res = this.generate_response("Flight successfully added.", flight);

    return of(res);
  }

  public GetFlights() : Observable<Flight[]> {
    return of(this._flights);
  }

  public AddPassenger(first_name:string, last_name:string, id: number, flight_class: number, sumOfTicket:number, flightNumber: number, NumberOfBags: number, TotalWeight: number) : Observable<ActionResponse<Passenger>> {

    if (this._passengers.some(x => x?.id == id)) {
      let error_res = this.generate_response("Some values submitted are invalid.", null,[{ property: "id", error_message: "Passenger id is already taken.", error_code:"idTaken"}]);
      return throwError(error_res);

    }





    let passenger: Passenger = {
      first_name: first_name,
      last_name: last_name,
      id: id,
      flight_class: flight_class,
      payment_amount: sumOfTicket,
      flight_number: flightNumber,
      number_of_bags: NumberOfBags,
      total_weight: TotalWeight,
      passenger_seat: undefined
    };

    //apply seat number

    let flight_class_data = FlightClass.get().find(x => x.id == passenger.flight_class);

    //get max by flight id and class
    let passenger_count_by_class = this._passengers.filter(x => x.flight_class == flight_class_data?.id && x.flight_number == passenger.flight_number).length;
    passenger.passenger_seat = passenger_count_by_class + (flight_class_data?.offset_seat ?? 0) + 1; //increment

    let passenger_validator = new PassengerValidator(this, passenger);
    let result = passenger_validator.validate_by_class_and_baggage();

    if (!result?.valid) {
      let error_res = this.generate_response("Some values submitted are invalid.", null,result?.errors);
      return throwError(error_res);
    }

    this._passengers.push(passenger);


    let res: ActionResponse<Passenger> = {
      message: "Passenger successfully added.",
      data: passenger
    };
    return of(res);
  }

  GetPassengers(id: number) : Observable<Passenger[]> {
    return of(this._passengers.filter(x => x.flight_number == id));
  }

  generate_response(message: string, data: any = null, errors: any = null) : ActionResponse<any> {
    let res: ActionResponse<any> = {
      message: "Passenger successfully added.",
      data: data,
      errors: errors
    };

    return res;
  }
}
