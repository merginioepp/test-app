export class Flight {

  flight_number: number | undefined;
  destination: string | undefined;

  constructor(_flight_number, _destination) {

    this.flight_number = _flight_number;
    this.destination = _destination;
  }

}
