export class ActionResponse<T> {
  message: string | undefined;
  data?: T | undefined;
  errors?: any;
}
