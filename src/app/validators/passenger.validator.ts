import { AppApiService } from "../app-api.service";
import { FlightClass } from "../models/flight-class.model";
import { Passenger } from "../models/passenger.model";

export class PassengerValidator {
  constructor(private _app_api_service : AppApiService, private _passenger: Passenger) {

  };

  public validate_by_class_and_baggage() {

    let errors:any = [];
    let flight_class = FlightClass.get().find(x => x.id == this._passenger.flight_class);

    if ((this._passenger.passenger_seat ?? 0) > ((flight_class?.max_seats ?? 0) + (flight_class?.offset_seat ?? 0))) errors.push({property: "flight_class", message: `Apologies. The ${flight_class?.description} of Flight # ${this._passenger.flight_number} is full of passengers.`, error_code:"passengerFull" });

    if ((this._passenger.number_of_bags ?? 0) > (flight_class?.baggage_count_limit ?? 0)) errors.push({property: "number_of_bags", message: `The ${flight_class?.description} of Flight # ${this._passenger.flight_number} baggage limit of ${flight_class?.baggage_count_limit} per passenger has been exceeded.`, error_code:"baggageExceed" });

    if ((this._passenger.total_weight ?? 0) > (flight_class?.baggage_weight_limit ?? 0)) errors.push({property: "total_weight", message: `The ${flight_class?.description} of Flight # ${this._passenger.flight_number} baggages weight limit of ${flight_class?.baggage_weight_limit} per passenger has been exceeded.`, error_code:"weightExceed" });

    return {
      valid: errors.length <= 0,
      errors: errors,
    }
  }
}
